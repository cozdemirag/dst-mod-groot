local assets=
{
	Asset("ANIM", "anim/ggwooddeluxe.zip"),

	Asset("IMAGE", "images/inventoryimages/ggwooddeluxe.tex"),	
	Asset("ATLAS", "images/inventoryimages/ggwooddeluxe.xml")
}

local function fn(Sim)
	local inst = CreateEntity()
	
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	MakeInventoryPhysics(inst)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst.entity:SetPristine()
	
	inst.AnimState:SetBank("ggwooddeluxe")
	inst.AnimState:SetBuild("ggwooddeluxe")
	inst.AnimState:PlayAnimation("idle")
	
	inst:AddComponent("inspectable")

	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "ggwooddeluxe"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/ggwooddeluxe.xml"
	inst.components.inventoryitem.atlas = resolvefilepath("images/inventoryimages/ggwooddeluxe.xml")
	
	inst:AddComponent("edible")
	inst.components.edible.foodtype = FOODTYPE.GROOTFOOD
	inst:AddTag("edible_"..FOODTYPE.GROOTFOOD)
	inst.components.edible.healthvalue = 16
    inst.components.edible.hungervalue = 68
	inst.components.edible.sanityvalue = 0
	
	inst:AddComponent("stackable")
	inst.components.stackable.maxsize = TUNING.STACK_SIZE_LARGEITEM
	
	--MakeHauntableLaunchAndDropFirstItem(inst)
	
	--inst:AddComponent("characterspecific")
	--inst.components.characterspecific:SetOwner("Groot")
    
	return inst
end

return Prefab( "common/inventory/ggwooddeluxe", fn, assets) 
