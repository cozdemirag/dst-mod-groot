# README #

*This is an unofficial fix for Dont Starve Together mod 'Groot'. 
*It only contains files fixes made for. You still require the rest of the files which can be found on mod's steam workshop link.
Link to original mod : https://steamcommunity.com/sharedfiles/filedetails/?id=391743403

All credits goes to the author mentioned on the mod's steam page. 

All I did is to fix few issues as the mod did not run on my copy of Dont Starve Together. (19th April 2018)

### What is changed/fixed? ###

* (modmain.lua) Fixes game world crashing on load
* (scripts/prefabs/ggwooddeluxe.lua) Updates WoodDeluxe items to stack up to 10. (Author seemed to want to do this judging by the comments in the code, yet was unable to implement it for whatever reason)

All the file paths mentioned above are relative to the mod's main folder in your <YOUR_ST_INSTALLATION>/mods/workshop-391743403/ directory.

### How do I use these fixes with the official mod? ###

* Subscribe to the original mod, let the steam/game download a copy of the mod to your machine.
* Find your Dont Starve Together installation directory
* Download the files with bugfixes either via this link https://bitbucket.org/cozdemirag/dst-mod-groot/get/2cfeb71beb4b.zip or use bitbucket if you know your way around it.
* Paste editted modmain.lua into <YOUR_ST_INSTALLATION>/mods/workshop-391743403/
* Paste editted ggwooddeluxe.lua into <YOUR_ST_INSTALLATION>/mods/workshop-391743403/scripts/prefabs/

### Credits ###

* All the credit goes to the author of the mod, as it goes by "SenLim" on Steam.
* Official mod's steam workshop link : https://steamcommunity.com/sharedfiles/filedetails/?id=391743403
