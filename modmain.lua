--[[
CREDITS
	Keyhandler...................................................Kzisor/Ysovuka
	Leveler......................................................Kzisor/Ysovuka
	LevelBadge...................................................Kzisor/Ysovuka
]]
-- Import the engine.
modimport("engine.lua")

-- Imports to keep the keyhandler from working while typing in chat.
Load "chatinputscreen"
Load "consolescreen"
Load "textedit"

local function GetModRPC(namespace, name)
	return MOD_RPC[namespace][name]
end

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

-- The character select screen lines
STRINGS.CHARACTER_TITLES.gggroot = "Groot"
STRINGS.CHARACTER_NAMES.gggroot = "Groot"
STRINGS.CHARACTER_DESCRIPTIONS.gggroot = "*Level System with Badge\n*Can only eat wood-type\n*Root to heal others"
STRINGS.CHARACTER_QUOTES.gggroot = "\"I Am ... Groot\""

-- Get config values
local config_treechop = GetModConfigData("gggroot_treechop") or .5
local config_chopbonus = GetModConfigData("gggroot_chopbonus") or "twigs"
local config_scarytobird = GetModConfigData("gggroot_scarytobird") or "notscary"
local config_sanitygiver = GetModConfigData("gggroot_sanityauraprefab") or "butterfly"
local config_sanityeffect = GetModConfigData("gggroot_sanityauraeffect") or 0
local config_wooddeluxerecipe = GetModConfigData("gggroot_wooddeluxe_difficulty") or "Normal"
local config_speech = GetModConfigData("gggroot_speech") or "groot"
local config_rootunrootpower = GetModConfigData("gggroot_rootunroot_power") or "Normal"
local config_chopexp = GetModConfigData("gggroot_chop_exp") or -0.1
local config_digexp = GetModConfigData("gggroot_dig_exp") or -0.1
--local config_plantexp = GetModConfigData("gggroot_plant_exp") or 1.0

--global to be used in gggroot.lua
GLOBAL.GGGROOT_SPEECH = config_speech
GLOBAL.GGGROOT_SCARYTOPREY = GetModConfigData("gggroot_scarytoprey") or "scary"
GLOBAL.GGGROOT_STARTINGSTATS = GetModConfigData("gggroot_startingstats") or "normal"
GLOBAL.GGGROOT_DMGMULTPERLEVELUP = GetModConfigData("gggroot_dmgmultperlevelup") or 0
GLOBAL.GGGROOT_ROOTUNROOTKEY = GetModConfigData("gggroot_rootunroot_key") or 122
GLOBAL.GGGROOT_MAXKILLEXP = GetModConfigData("gggroot_max_kill_exp") or 200
GLOBAL.GGGROOT_PCTOVERFLOWONKILL = GetModConfigData("gggroot_pctoverflowonkill") or .5
GLOBAL.GGGROOT_LEVEL_SYSTEM = "GrootLevelSystem"

PrefabFiles = {
	"gggroot"
	,"ggwooddeluxe"
}

Assets = {
    Asset("IMAGE", "bigportraits/gggroot.tex"),
    Asset("ATLAS", "bigportraits/gggroot.xml"),
	Asset("IMAGE", "images/avatars/avatar_gggroot.tex"),
    Asset("ATLAS", "images/avatars/avatar_gggroot.xml"),
	Asset("IMAGE", "images/avatars/avatar_ghost_gggroot.tex"),
    Asset("ATLAS", "images/avatars/avatar_ghost_gggroot.xml"),
	Asset("IMAGE", "images/map_icons/gggroot.tex"),
	Asset("ATLAS", "images/map_icons/gggroot.xml"),
	Asset("IMAGE", "images/saveslot_portraits/gggroot.tex"),
    Asset("ATLAS", "images/saveslot_portraits/gggroot.xml"),
    Asset("IMAGE", "images/selectscreen_portraits/gggroot.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/gggroot.xml"),
    Asset("IMAGE", "images/selectscreen_portraits/gggroot_silho.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/gggroot_silho.xml"),
	Asset("ANIM", "anim/gggrootlevelbadge.zip"),
	Asset("ANIM", "anim/progressbar.zip"),
}

if config_speech == "groot" then
	STRINGS.CHARACTERS.GGGROOT = require "speech_gggroot"
else
	STRINGS.CHARACTERS.GGGROOT = require "speech_gggroot_eng"
end

-- The character's name as appears in-game 
STRINGS.NAMES.GGGROOT = "Groot"

-- The default responses of examining the character
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GGGROOT = 
{
	GENERIC = "Ooh ooh! *I Am Groot*",
	ATTACKER = "That Groot looks shifty...",
	MURDERER = "Groot the Murderer!",
	REVIVER = "Groot the Reviver",
	GHOST = "Hauntingly Groot",
}

--MALE, FEMALE, ROBOT, NEUTRAL or PLURAL
table.insert(GLOBAL.CHARACTER_GENDERS.NEUTRAL, "gggroot")

--Planting pinecone = gain sanity
--Error: CRASHES CLIENT
--[[
local function pineconeMod(inst)
	inst.old_ondeploy = inst.components.deployable.ondeploy
	
	inst.components.deployable.ondeploy = function(inst, pt, doer)
		inst:old_ondeploy(pt)
		doer.components.sanity:DoDelta(3)
	end
end
AddPrefabPostInit("pinecone", pineconeMod)
--]]

--What kind of Food can Groot eats
GLOBAL.FOODTYPE.GROOTFOOD = "GROOTFOOD"
local groot_food = {"twigs", "log", "boards", "petals", "petals_evil", "pinecone", "cutgrass", "foliage", "cutlichen", "livinglog"}
local function AddGrootFood(inst)
    inst:AddTag("edible_"..GLOBAL.FOODTYPE.GROOTFOOD)
end
for k,v in pairs(groot_food) do
    AddPrefabPostInit(v, AddGrootFood)
end
GLOBAL.FOODGROUP.GROOT_FOODGROUP = {
	name = "GROOT_FOODGROUP"
	,types = {
		GLOBAL.FOODTYPE.GROOTFOOD,
	}
}

--Chopping trees effectiveness (configurable)
AddComponentPostInit("workable", function(self)
	local old_WorkedBy = self.WorkedBy
	function self:WorkedBy(worker, numworks)
		if worker.prefab == "gggroot" and self.action == GLOBAL.ACTIONS.CHOP then
			numworks = numworks * config_treechop or .5
		end
		return old_WorkedBy(self, worker, numworks)
	end
end)

--Can only say "I Am Groot"
if config_speech == "groot" then
	AddComponentPostInit("talker", function(self)
		local old_Say = self.Say
		function self:Say(script, ...)
			if self.inst.prefab == "gggroot" then
				if script and type(script) == "string" then
					script = script:match("^%$%$(.*)$") or "I Am Groot"
				end
			end
			return old_Say(self, script, ...)
		end
	end)
end

--Chopping tree gets a bonus (configurable)
local function EvergreenPostInit(inst)
	if GLOBAL.TheWorld.ismastersim then
		local oldonfinish = inst.components.workable.onfinish
		inst.components.workable.onfinish = function(inst, chopper)
			if chopper and chopper.prefab == "gggroot" then
				--table.insert(inst.components.lootdropper.loot, "twigs") --this accumulates, do not use
				inst.components.lootdropper:SpawnLootPrefab(config_chopbonus)
			end
			oldonfinish(inst, chopper)
		end
	end
end
local trees = {"evergreen", "evergreen_normal", "evergreen_tall", "evergreen_short", "evergreen_sparse", "evergreen_sparse_normal", "evergreen_sparse_tall", "evergreen_sparse_short", "deciduoustree", "deciduoustree_normal", "deciduoustree_tall", "deciduoustree_short"}
for k,v in pairs(trees) do AddPrefabPostInit(v, EvergreenPostInit) end

AddMinimapAtlas("images/map_icons/gggroot.xml")
AddModCharacter("gggroot")
AddPrefabPostInit("ggwooddeluxe")

local Recipe = GLOBAL.Recipe
local RECIPETABS = GLOBAL.RECIPETABS
local TECH = GLOBAL.TECH

if config_wooddeluxerecipe == "VEasy" then
	local recipes = {Recipe("ggwooddeluxe", {Ingredient("twigs",1),Ingredient("log",1),Ingredient("cutgrass",1),}
							,RECIPETABS.FARM --Food
							,{SCIENCE=0},nil,nil,nil,nil,"ggwooddeluxe_builder"),
					}
	for k,v in pairs(recipes) do
		v.atlas = "images/inventoryimages/" .. v.name .. ".xml"
	end
elseif config_wooddeluxerecipe == "Easy" then
	local recipes = {Recipe("ggwooddeluxe", {Ingredient("twigs",3),Ingredient("log",1),Ingredient("boards",1),}
							,RECIPETABS.FARM --Food
							,{SCIENCE=0},nil,nil,nil,nil,"ggwooddeluxe_builder"),
					}
	for k,v in pairs(recipes) do
		v.atlas = "images/inventoryimages/" .. v.name .. ".xml"
	end
elseif config_wooddeluxerecipe == "Normal" then
	local recipes = {Recipe("ggwooddeluxe", {Ingredient("twigs",6),Ingredient("log",3),Ingredient("boards",1),}
							,RECIPETABS.FARM --Food
							,{SCIENCE=1},nil,nil,nil,nil,"ggwooddeluxe_builder"),
					}
	for k,v in pairs(recipes) do
		v.atlas = "images/inventoryimages/" .. v.name .. ".xml"
	end
elseif config_wooddeluxerecipe == "Hard" then
	local recipes = {Recipe("ggwooddeluxe", {Ingredient("twigs",6),Ingredient("log",4),Ingredient("boards",2),}
							,RECIPETABS.FARM --Food
							,{SCIENCE=2},nil,nil,nil,nil,"ggwooddeluxe_builder"),
					}
	for k,v in pairs(recipes) do
		v.atlas = "images/inventoryimages/" .. v.name .. ".xml"
	end
else --Brutal
	local recipes = {Recipe("ggwooddeluxe", {Ingredient("twigs",9),Ingredient("log",6),Ingredient("boards",2),}
							,RECIPETABS.FARM --Food
							,{SCIENCE=2},nil,nil,nil,nil,"ggwooddeluxe_builder"),
					}
	for k,v in pairs(recipes) do
		v.atlas = "images/inventoryimages/" .. v.name .. ".xml"
	end
end

STRINGS.NAMES.GGWOODDELUXE = "Wood Deluxe"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GGWOODDELUXE = "Delicious looking wood gourmet..."
STRINGS.RECIPE_DESC.GGWOODDELUXE = "More filling than a log"

--Birds don't fly away (configurable)
if config_scarytobird == "notscary" then
	local function RecheckForThreat(inst)
		local busy = inst.sg:HasStateTag("sleeping") or inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("flying")
		if not busy then
			local threat = GLOBAL.FindEntity(inst, 5, nil, nil, {'notarget', 'birdwhisperer'}, {'player', 'monster', 'scarytoprey'})
			return threat ~= nil or GLOBAL.TheWorld.state.isnight
		end
	end
	 
	AddStategraphPostInit("bird", function(sg)
		local old = sg.events.flyaway.fn
		sg.events.flyaway.fn = function(inst)
			if RecheckForThreat(inst) then
				old(inst)
			end
		end
	end)
end

--Butterfly gives tiny sanity (configurable)
if config_sanityeffect ~= 0 then
	AddPrefabPostInit(config_sanitygiver, function(inst)
		if not GLOBAL.TheWorld.ismastersim then
			return inst
		end
		if not inst.components.sanityaura then
			inst:AddComponent("sanityaura")
		end
		inst.components.sanityaura.aurafn = function(aurer, observer)
			if observer.prefab == "gggroot" then
				return 100/(30*config_sanityeffect)
			else
				return 0
			end
		end
	end)
end

local old_Eat = GLOBAL.ACTIONS.EAT.fn
GLOBAL.ACTIONS.EAT.fn = function(act)
	if act.doer.prefab == "gggroot" and act.doer.transformed == true then
		return --can't eat while rooted
	end
	return old_Eat(act)
end

local old_UseItem = GLOBAL.ACTIONS.USEITEM.fn
GLOBAL.ACTIONS.USEITEM.fn = function(act)
	if act.doer.prefab == "gggroot" and act.doer.transformed == true then
		return --can't use item while rooted
	end
	return old_UseItem(act)
end

local function dorootedhealtask(inst)
	local healradius = 0
	local heal_health = 0
	local heal_sanity = 0
	
	if config_rootunrootpower == "Weakest" then
		healradius = 20
		heal_health = 1
		heal_sanity = 1
	elseif config_rootunrootpower == "Weaker" then
		healradius = 40
		heal_health = 2
		heal_sanity = 2
	elseif config_rootunrootpower == "Normal" then
		healradius = 60
		heal_health = 2
		heal_sanity = 2
	elseif config_rootunrootpower == "Stronger" then
		healradius = 80
		heal_health = 3
		heal_sanity = 3
	elseif config_rootunrootpower == "Strongest" then
		healradius = 100
		heal_health = 4
		heal_sanity = 4
	else --Cheater
		healradius = 200
		heal_health = 10
		heal_sanity = 10
	end
	
	local healer = inst
	local x,y,z = healer.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x,y,z,healradius)
	
	--find all mobs within radius of healer
	for i, mob in ipairs(ents) do
		--exclude healer (self), structure, eyeplant, walls
		--must be visible, must have health and locomotor components
		if mob ~= healer and not mob:HasTag("structure") and not mob:HasTag("eyeplant") and not mob:HasTag("wall") and mob.entity:IsVisible() and mob.components.health ~= nil and mob.components.locomotor ~= nil then
			
			--if the mob has combat component and has target (ie: angry), but exclude player tag
			if mob.components.combat and mob.components.combat.target and not mob:HasTag("player") then
				mob.components.combat:SuggestTarget(healer)
			else
			--else mob must be a player, but exclude ghost player
				if not mob:HasTag("playerghost") and mob:HasTag("player") then
					--heal sanity and health if they are within healradius
					if inst:GetDistanceSqToInst(mob) < healradius then
						if mob.components.sanity then
							mob.components.sanity:DoDelta(heal_health)
						end
						if mob.components.health then
							mob.components.health:DoDelta(heal_sanity)
						end
					end
				end
			end
			
		end --end if
	end --end for
end

local function RootUnrootFn(inst)
	if inst:HasTag("playerghost") then return end --ignore if ghost
	if inst.sg:HasStateTag("busy") then return end --ignore if busy
	
	if GLOBAL.KnownModIndex:IsModEnabled("workshop-398858801") then --AFK Detection
		if inst.components.afk ~= nil then
			inst.components.afk:ResetAFKTime(false)
		end
	end
	
	local healfreq = 0
	local rooteddmgabsorb = 0
	local hungerdrainratemult = 0
	
	if config_rootunrootpower == "Weakest" then
		healfreq = 4
		rooteddmgabsorb = 0.2
		hungerdrainratemult = 7
	elseif config_rootunrootpower == "Weaker" then
		healfreq = 3
		rooteddmgabsorb = 0.5
		hungerdrainratemult = 6.5
	elseif config_rootunrootpower == "Normal" then
		healfreq = 2
		rooteddmgabsorb = 0.7
		hungerdrainratemult = 6
	elseif config_rootunrootpower == "Stronger" then
		healfreq = 1
		rooteddmgabsorb = 0.75
		hungerdrainratemult = 5.5
	elseif config_rootunrootpower == "Strongest" then
		healfreq = 1
		rooteddmgabsorb = 0.8
		hungerdrainratemult = 5
	else --Cheater
		healfreq = 1
		rooteddmgabsorb = 0.99
		hungerdrainratemult = 2
	end
	
	--rooted
	if not inst.transformed then
		--inst.AnimState:SetBuild("grootrooted")
		local x, y, z = inst.Transform:GetWorldPosition()
		local fx = GLOBAL.SpawnPrefab("groundpound_fx")
		inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
		fx.Transform:SetPosition(x, y, z)
		GLOBAL.SpawnPrefab("collapse_big").Transform:SetPosition(inst:GetPosition():Get())
		GLOBAL.SpawnPrefab("mole_move_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
		
		--for i, v in ipairs(GLOBAL.AllPlayers) do
		--	v:ShakeCamera(GLOBAL.CAMERASHAKE.FULL, .7, .02, .3, inst, 40) --does not seem to work?
		--end
		
		inst.AnimState:SetMultColour(0,1,0,1) --green
		
		if inst.components.locomotor then
			inst.components.locomotor:Stop()
			inst.components.locomotor:SetExternalSpeedMultiplier(inst, inst, 0.01)
		end
		
		if inst.components.health then
			inst.components.health.absorb = rooteddmgabsorb
			inst.components.health:SetInvincible(false) --in case other mod setting this to true
		end
		
		if inst.components.hunger then
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * hungerdrainratemult)
			inst.components.hunger:Resume()
		end
		
		if inst.rootedhealtask == nil then
			inst.rootedhealtask = inst:DoPeriodicTask(healfreq, dorootedhealtask)
		end
		
		if inst.components.combat then
			inst.components.combat.canattack = false
			inst.components.combat:SetTarget(nil)
		end
		
		inst:SetStateGraph("SGwilsonghost")
		inst.sg:GoToState("idle")
		
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller:Enable(false)
		end
		
		if not inst.AnimState:IsCurrentAnimation("idle") then
			inst.AnimState:PlayAnimation("idle", true)
		end
		
	--normal (unrooted)
	else
		--inst.AnimState:SetBuild("groot")
		local x, y, z = inst.Transform:GetWorldPosition()
		local fx = GLOBAL.SpawnPrefab("groundpound_fx")
		inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
		fx.Transform:SetPosition(x, y, z)
		GLOBAL.SpawnPrefab("collapse_big").Transform:SetPosition(inst:GetPosition():Get())
		
		inst.AnimState:SetMultColour(1,1,1,1)
		
		if inst.components.locomotor then
			inst.components.locomotor:Stop()
			inst.components.locomotor:SetExternalSpeedMultiplier(inst, inst)
		end
		
		if inst.components.health then
			inst.components.health.absorb = 0 --must match with gggroot.lua!
			inst.components.health:SetInvincible(false) --in case other mod setting this to true
		end
		
		if inst.components.hunger then
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 2) --must match with gggroot.lua!
			inst.components.hunger:Resume()
		end
		
		if inst.rootedhealtask ~= nil then
			inst.rootedhealtask:Cancel()
			inst.rootedhealtask = nil
		end
		
		if inst.components.combat then
			inst.components.combat.canattack = true
		end
		
		inst:SetStateGraph("SGwilson")
		inst.sg:GoToState("idle")
		
		if inst.components.playercontroller ~= nil then
			inst.components.playercontroller:Enable(true)
		end
	 
	end
	 
	inst.transformed = not inst.transformed

	return true
end

AddModRPCHandler("Groot", "RootUnroot", RootUnrootFn)

AddReplicableComponent("leveler")

local function MakeLevelBadge( self, badge_name, owner_tag, level_system, bank, build, background_build, show_progress, show_remaining, pos, pos_mod )

	if self.owner:HasTag(owner_tag) then
		local LevelBadge = require "widgets/levelbadge"
		self[badge_name] = self:AddChild(LevelBadge(self.owner, level_system, bank, build, background_build, show_progress, show_remaining))

		-- Compatibility with Always On Status mod
		if GLOBAL.KnownModIndex:IsModEnabled("workshop-376333686") then			
			self[badge_name]:SetPosition(pos_mod.x, pos_mod.y)
		else	
	    	self[badge_name]:SetPosition(pos.x, pos.y)
		    self.brain:SetPosition(-40,-55,0) --move sanity badge
		   	self.moisturemeter:SetPosition(0,-120,0) --move moisturemeter badge
		end

		local function leveldelta( data )
			self[badge_name]:SetPercent( data.newpercent, data.maxexperience, data.currentlevel, data.maxlevel )
		end

		local function OnSetPlayerMode( self )
			if self["on" .. level_system .. "delta"] == nil then
				self["on" .. level_system .. "delta"] = function( owner, data ) leveldelta( data ) end
				self.inst:ListenForEvent( "leveldelta", self["on" .. level_system .. "delta"], self.owner )
				if self.owner.replica.leveler then
					leveldelta( { newpercent = self.owner.replica.leveler:GetPercent( level_system ),
						maxexperience = self.owner.replica.leveler:GetMaxExperience( level_system ),
						currentlevel = self.owner.replica.leveler:GetLevel( level_system ),
						maxlevel = self.owner.replica.leveler:GetMaxLevel( level_system ) })
				end
			end
		end

		local function OnSetGhostMode( self )
			if self["on" .. level_system .. "delta"] ~= nil then
				self.inst:RemoveEventCallback( "leveldelta", self["on" .. level_system .. "delta"], self.owner )
				self["on" .. level_system .. "delta"] = nil
			end
		end

		local _SetGhostMode = self.SetGhostMode

		function self:SetGhostMode( ghostmode )
			_SetGhostMode( self, ghostmode )
			if ghostmode then
				self[badge_name]:Hide()
				OnSetGhostMode( self )
			else
				self[badge_name]:Show()
				OnSetPlayerMode( self )
			end
		end

		OnSetPlayerMode( self )

	end
	
	return self

end

local function StatusDisplaysPostInit( self )
	--MakeLevelBadge
	----self, badge name, owner tag, level system name
	----,badge icon animation bank, badge icon animation build
	----,background animation build
	----,true/false for show progress
	----,true/false for show remaining (default is false)
	----,position (x,y,z) normal
	----,position (x,y,z) modded
	MakeLevelBadge(self,"gggroot_exp", "gggroot", "GrootLevelSystem", "icon", "gggrootlevelbadge", "progressbar", true, false, {x=40,y=-55,z=0}, {x=-62,y=-52,z=0})
end

AddClassPostConstruct("widgets/statusdisplays", StatusDisplaysPostInit)

--chop action loses exp
local function ExpOnChop(inst)
	if inst.components.leveler then
		local currexp = inst.components.leveler:GetExperience(GLOBAL.GGGROOT_LEVEL_SYSTEM)
		if config_chopexp >= 0 or currexp+config_chopexp >= 0 then --do nothing if it would cause it to be negative
			inst.components.leveler:DoDelta(GLOBAL.GGGROOT_LEVEL_SYSTEM, config_chopexp)
		end
	end
end
AddModRPCHandler("Groot", "ChopExp", ExpOnChop)
local old_Chop = GLOBAL.ACTIONS.CHOP.fn
GLOBAL.ACTIONS.CHOP.fn = function(act)
	if act.doer:HasTag("gggroot") then
		if not act.doer.transformed then
			SendModRPCToServer(GetModRPC("Groot", "ChopExp"))
		end
	end	
	return old_Chop(act)
end

--dig action loses exp
local function ExpOnDig(inst)
	if inst.components.leveler then
		local currexp = inst.components.leveler:GetExperience(GLOBAL.GGGROOT_LEVEL_SYSTEM)
		if config_digexp >= 0 or currexp+config_digexp >= 0 then --do nothing if it would cause it to be negative
			inst.components.leveler:DoDelta(GLOBAL.GGGROOT_LEVEL_SYSTEM, config_digexp)
		end
	end
end
AddModRPCHandler("Groot", "DigExp", ExpOnDig)
local old_Dig = GLOBAL.ACTIONS.DIG.fn
GLOBAL.ACTIONS.DIG.fn = function(act)
	if act.doer:HasTag("gggroot") then
		if not act.doer.transformed then
			SendModRPCToServer(GetModRPC("Groot", "DigExp"))
		end
	end	
	return old_Dig(act)
end

-- --plant action gains exp
-- local function ExpOnPlant(inst)
	-- if inst.components.leveler then
		-- local currexp = inst.components.leveler:GetExperience(GLOBAL.GGGROOT_LEVEL_SYSTEM)
		-- if config_plantexp >= 0 or currexp+config_plantexp >= 0 then --do nothing if it would cause it to be negative
			-- print ("Planted, gain xp: " .. config_plantexp)
			-- inst.components.leveler:DoDelta(GLOBAL.GGGROOT_LEVEL_SYSTEM, config_plantexp)
		-- end
	-- end
-- end
-- AddModRPCHandler("Groot", "PlantExp", ExpOnPlant)
-- local old_Plant = GLOBAL.ACTIONS.PLANT.fn
-- GLOBAL.ACTIONS.PLANT.fn = function(act)
	-- if act.doer:HasTag("gggroot") then
		-- if not act.doer.transformed then
			-- SendModRPCToServer(GetModRPC("Groot", "PlantExp"))
			
			-- --add 1 sanity
			-- if act.doer.components.sanity then
				-- act.doer.components.sanity:DoDelta(1, false)
			-- end
		-- end		
	-- end	
	-- return old_Plant(act)
-- end